# frozen_string_literal: true

require_relative 'base'
require_relative 'shared/issuable'

module Gitlab
  module Triage
    module Resource
      class Issue < Base
        include Shared::Issuable

        def merge_requests_count
          @merge_requests_count ||= resource.dig(:merge_requests_count)
        end

        def related_merge_requests
          @related_merge_requests ||= network.query_api_cached(
            resource_url(sub_resource_type: 'related_merge_requests'))
            .map { |merge_request| MergeRequest.new(merge_request, parent: self) }
        end

        def closed_by
          @closed_by ||= network.query_api_cached(
            resource_url(sub_resource_type: 'closed_by'))
            .map { |merge_request| MergeRequest.new(merge_request, parent: self) }
        end
      end
    end
  end
end
